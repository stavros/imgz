import datetime
from typing import Optional

from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.decorators.http import require_POST
from loginas.utils import restore_original_login

from .models import Image
from .models import User
from .utils import process_upload
from .utils import resize_image
from .utils import UploadError


def get_image_response(image: bytes, format: str) -> HttpResponse:
    """Return an HttpResponse for a given image."""
    # We cast to bytes here because this is a memoryview on Postgres
    # but just bytes on SQLite.
    data = bytes(image)
    response = HttpResponse(data, content_type=f"image/{format}")
    response["Content-Length"] = len(data)
    return response


def get_image(image_id: str) -> Image:
    image = get_object_or_404(Image, pk=image_id)
    return image


def delete_all_their_shit(user: User) -> None:
    """
    Delete all the user's shit.
    """
    for image in Image.objects.filter(user=user):
        image.delete()

    user.upgraded_until = datetime.date(2000, 1, 1)
    user.storage_space = 0
    user.bonus_space = 0
    user.save()


@login_required
def logout(request):
    """
    Log the user out.

    This is a custom logout view that restores the original user with django-loginas.
    """
    restore_original_login(request)
    return redirect(settings.LOGOUT_REDIRECT_URL)


@login_required
def account(request):
    if request.method == "POST":
        if request.GET.get("delete") == "allmyshit":
            delete_all_their_shit(request.user)
            messages.success(request, "Good riddance.")
            logout(request)
        elif request.GET.get("unsub") == "scribeme":
            request.user.stop_stripe_subscription()
            messages.success(
                request, "Okay your subscription has been canceled. I think."
            )
        return redirect("main:index")

    return render(request, "account.html")


def api_docs(request: HttpRequest) -> HttpResponse:
    api_key = request.user.api_key if request.user.is_authenticated else "y0uRAP1k3y"
    return render(request, "api.html", {"api_key": api_key})


def latest(request: HttpRequest) -> HttpResponse:
    if not request.user.is_superuser:
        raise Http404()
    try:
        items = int(request.GET.get("items", ""))
    except Exception:
        items = 100

    return render(
        request,
        "latest.html",
        {
            "images": Image.objects.defer("data", "thumbnail_512").order_by(
                "-uploaded"
            )[:items]
        },
    )


def index(request: HttpRequest) -> HttpResponse:
    if not request.user.is_authenticated:
        if request.GET.get("v") == "s":
            return render(request, "simple_landing.html")
        elif request.GET.get("v") == "c":
            return render(request, "complex_landing.html")
        else:
            return render(request, "index.html")

    if not request.user.is_upgraded:
        return render(request, "unpaid.html")

    return render(
        request,
        "home.html",
        {
            "images": request.user.images.defer("data", "thumbnail_512").order_by(
                "-uploaded"
            )
        },
    )


def image_show_resized(
    request: HttpRequest, image_id: str, size: str, extension: Optional[str] = None
) -> HttpResponse:
    """
    Show a resized image.
    """
    image = get_image(image_id)
    s = int(size)
    if s != 1280:
        return HttpResponseNotFound("Size not found.")

    data = resize_image(image.data, s)
    return get_image_response(data, image.format)


def image_show_thumbnail(
    request: HttpRequest, image_id: str, size: str, extension: Optional[str] = None
) -> HttpResponse:
    """
    Show an image's thumbnail.
    """
    image = get_image(image_id)
    s = int(size)
    if s != 512:
        return HttpResponseNotFound("Thumbnail not found.")

    return get_image_response(image.thumbnail_512, image.format)


def image_page(
    request: HttpRequest, image_id: str, extension: Optional[str] = None
) -> HttpResponse:
    """
    Show an image page.
    """
    image = get_image(image_id)

    if request.method == "POST":
        if image.user != request.user:
            messages.error(request, "No.")
            return redirect(image)

        try:
            image.set_title(request.POST.get("title"))
        except ValueError as e:
            messages.error(request, str(e))
            return redirect(image)

        image.save()
        messages.success(
            request, "The previous title sucked, huh? Well this one isn't any better."
        )
        return redirect(image)

    image.increment_views()
    return render(
        request, "image.html", {"image": image, "nopreview": request.GET.get("np")}
    )


def image_show(
    request: HttpRequest, image_id: str, extension: Optional[str] = None
) -> HttpResponse:
    """
    Show a bare image.
    """
    image = get_image(image_id)
    return get_image_response(image.data, image.format)


@login_required  # type: ignore
@require_POST  # type: ignore
def image_delete(
    request: HttpRequest, image_id: str, extension: Optional[str] = None
) -> HttpResponse:
    image = get_image(image_id)
    if image.user != request.user and not request.user.is_superuser:
        raise Http404
    image.delete()
    messages.success(request, "Okay, the image is gone. It was a dick pic, wasn't it?")
    return redirect("main:index")


@login_required  # type: ignore
def image_upload(request: HttpRequest) -> HttpResponse:
    class ImageUploadForm(forms.Form):
        EXPIRATION = [
            [10, "ten minutes"],
            [60, "an hour"],
            [24 * 60, "a day"],
            [7 * 24 * 60, "a week"],
            [14 * 24 * 60, "two weeks"],
            [30 * 24 * 60, "a month"],
            [None, "never"],
        ]

        title = forms.CharField(
            max_length=200, help_text="The title of the image.", required=False
        )
        expires = forms.ChoiceField(
            choices=EXPIRATION, initial=None, label="Expires in", required=False
        )

        image = forms.FileField()

    if request.method == "POST":
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                image = process_upload(
                    request.FILES,
                    request.user,
                    title=form.cleaned_data["title"],
                    expires_in=int(form.cleaned_data["expires"])
                    if form.cleaned_data["expires"]
                    else None,
                )
            except UploadError as e:
                messages.error(request, str(e))
                return redirect("main:image-upload")
            else:
                return redirect(image)
    else:
        form = ImageUploadForm()
    return render(request, "upload.html", {"form": form})
