# Generated by Django 2.2.7 on 2019-11-20 16:00
import datetime

from django.db import migrations
from django.db import models


class Migration(migrations.Migration):
    dependencies = [("main", "0014_auto_20191120_1538")]

    operations = [
        migrations.AddField(
            model_name="user",
            name="last_payment",
            field=models.DateField(default=datetime.date(1900, 1, 1)),
        )
    ]
