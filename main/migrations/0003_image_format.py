# Generated by Django 2.2.7 on 2019-11-18 22:05
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):
    dependencies = [("main", "0002_image")]

    operations = [
        migrations.AddField(
            model_name="image",
            name="format",
            field=models.CharField(blank=True, max_length=100),
        )
    ]
